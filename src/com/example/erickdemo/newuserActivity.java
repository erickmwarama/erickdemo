package com.example.erickdemo;

import android.app.Activity;
import android.os.Bundle;
import android.widget.*;
import android.view.View;
import android.content.ContentValues;
import android.database.sqlite.*;

public class newuserActivity extends Activity
{
	
	private TextView message;
	private Button submit;
	private EditText name, email;
	private RadioGroup gender;
	private String n,e,g;
	public static SQLiteDatabase sqldb;
	
	
	public void onCreate(Bundle state)
	{
		super.onCreate(state);
		setContentView(R.layout.newuser);
		message=(TextView)findViewById(R.id.messageview);
		submit=(Button)findViewById(R.id.submituser);
		name=(EditText)findViewById(R.id.nametext);
		email=(EditText)findViewById(R.id.emailtext);
		gender=(RadioGroup)findViewById(R.id.gendergroup);
		sqldb=openOrCreateDatabase("usersdb.db",SQLiteDatabase.CREATE_IF_NECESSARY,null);
		try{
		sqldb.execSQL("CREATE TABLE newUSERS(NAME TEXT, EMAIL_id TEXT, GENDER TEXT);");
		}catch(SQLiteException s){}
	
	
	submit.setOnClickListener(new View.OnClickListener(){
		public void onClick(View v)
		{
			//message.setText("you have submitted");
			n=name.getText().toString();
			e=email.getText().toString();
			
			sendtodatabase();
			message.setText("values sent to database");
			name.setText("");
			email.setText("");
		}
	});
	
	gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
		
		@Override
		public void onCheckedChanged(RadioGroup arg0, int arg1) {
			switch(arg1)
			{
			case R.id.gendermale:
				g="MALE";
			case R.id.genderfemale:
				g="FEMALE";
			
			}
				
			
		}
	});
	
	}
	
	private void sendtodatabase()
	{
		ContentValues cv=new ContentValues();
		cv.put("NAME", n);
		cv.put("EMAIL_id", e);
		cv.put("GENDER", g);
		sqldb.insert("newUSERS", null, cv);
	}
	
	
	
}
package com.example.erickdemo;

import android.app.Activity;
import android.os.Bundle;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.widget.TextView;
import android.widget.Button;
import android.view.View;
import android.content.Intent;

public class ErickmainActivity extends Activity {

    //private TextView title;
    private Button create, display;
	
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.erickmain);
       // title=(TextView)findViewById(R.id.titleview);
        create=(Button)findViewById(R.id.createbutton);
        display=(Button)findViewById(R.id.displaybutton);
        
        create.setOnClickListener(new View.OnClickListener(){
        	public void onClick(View v)
        	{
        		Intent nuser=new Intent(getBaseContext(),newuserActivity.class);
        		startActivity(nuser);
        		//title.setText("you have clicked create");
        	}
        });
        
        display.setOnClickListener(new View.OnClickListener(){
        	public void onClick(View v)
        	{
        		Intent disp=new Intent(getBaseContext(),displayusers.class);
        		startActivity(disp);
        	}
        });
    }
}
